#from django.shortcuts import render
from django.http import HttpResponse, Http404
from .models import Contenido
from django.views.decorators.csrf import csrf_exempt
# Create your views here.

FORM = """
<form action="" method="POST">
<h1>No existe ese valor en la base de datos</h1>
<p>Introduce ese valor a la base de datos</p>
    <div>
    Valor: <input type="text" name="valor">
    </div>
    <div>
    <input type="submit" value="GUARDAR">
    </div>
</form>
"""

@csrf_exempt
def get_content(request, llave):
    #si PUT guardamos el valor en la base de datos y la mostramos
    if request.method == 'PUT':
        valor = request.body.decode('utf-8')
        c = Contenido(clave=llave, valor=valor)
        c.save()
        response = "Datos guardados"

    #si POST guardamos el valor que nos pasen despues de decodificarlo en la tabla de contenidos con clave=llave y el valor que sera el cuerpo
    if request.method == "POST":
        body = request.POST['valor']
        c = Contenido(clave=llave, valor=body)
        #lo guardamos en la base de datos
        c.save()
        response = "Datos guardados"

    if request.method == "GET":
        fila = Contenido.objects.get(clave=llave)
        response = fila.valor
    #si GET,
    #try:
        # Contenido es una linea de la tabla de contenidos,
        # la tabla tiene 3 columnas: clave, valor e id, en este caso se mete en la parte de claves y con el get
        # busca la clave llave
        #contenido = Contenido.objects.get(clave=llave)
        #response = "La llave es: " + llave + "y el valor es:" + contenido.valor + "y su id es: " + str(contenido.id)
    #except Contenido.DoesNotExist:
        #raise FORM
    return HttpResponse(response)
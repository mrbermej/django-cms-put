from . import views
from django.urls import path

urlpatterns = [
    path('<str:llave>', views.get_content)
]